package estructuras;

import java.util.*;

/**
 *  La clase <tt>MapArbolRecubrimiento</tt> permite obtener el arbol 
 *  de recubrimiento a partir de un nodo origen.
 *  <p>
 *  Dicho arbol permite conocer cuáles nodos se pueden visitar saliendo de 
 *  el nodo origen.
 *  <b>Para obtener mayor información:</b>
 *  Consultar la <a href="http://algs4.cs.princeton.edu/41graph/">Sección 4.1</a> de
 *  <i>Algorithms, 4th Edition</i> de Robert Sedgewick y Kevin Wayne.
 *
 *  @author ISIS1206 Team
 */
public class MapArbolRecubrimiento {

	/**
	 * Referencia al MapGraph para el que se quiere encontrar el camino más corto entre dos nodos
	 */
	private MapGraph mapGraph;

	/**
	 * Lista en la que se guardan los nodos a los que se puede llegar
	 */
	private ArrayList<Integer> lista;

	/**
	 * Constructor de la clase que realiza la búsqueda del arbol de recubrimiento
	 * @param mapGraph Referencia al MapGraph sobre el que se quiere realizar el algoritmo
	 * @param idSource Id de la fuente
	 * @param idDestination Id del destino
	 */
	public MapArbolRecubrimiento(MapGraph mapGraph, int idOrigen)
	{
		this.mapGraph = mapGraph;
		lista = new ArrayList<Integer>();

		Stack<Integer> nodos = new Stack<Integer>();
		nodos.add(idOrigen);
		lista.add(idOrigen);


		while(true)
		{
			if(nodos.isEmpty())
			{
				break;
			}
			else
			{
				//TODO 3). Complete el método. Debe tomar el primer elemento del stack, verificar en 
				// sus edges los nodos de destino y agregar a la lista los id's de los nodos 
				// que no se encuentren repetidos
				
				
			}
		}

	}

	public ArrayList<MapNode> getAnswer()
	{
		ArrayList <MapNode> answer = new ArrayList<>();
		for (Integer integer : lista) {
			answer.add(mapGraph.getMapNode(integer));
		}
		return answer;
	}

}


