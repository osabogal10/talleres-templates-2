package taller.estructuras;


public class ListaDobleEncadenada<T> {

	private NodoLista<T> primero;
	private NodoLista<T> ultimo;


	public ListaDobleEncadenada()
	{
		primero = null;
		ultimo = null;
	}

	public int size() 
	{
		int cont = 0;
		NodoLista<T> act = primero;

		while(act!=null)
		{
			cont++;
			act = act.getNext();
		}
		return cont;
	}

	public boolean isEmpty()
	{
		return primero == null;
	}

	public void addFirst (T producto)
	{
		NodoLista<T> act = primero;
		NodoLista<T> nuevo = new NodoLista<T>(producto);

		if (primero!=(null))
		{

			while (act.getPrev()!=null)
			{
				act = act.getPrev();
			}

			act.setPrev(nuevo);
			nuevo.setNext(act);

		}
		else
		{
			primero = nuevo;
			ultimo = nuevo;
		}
	}

	public void addLast(T nElem)
	{
		NodoLista<T> act = primero;
		NodoLista<T> nuevo = new NodoLista<T>(nElem);

		if (primero!=(null))
		{

			while (act.getNext()!=null)
			{
				act = act.getNext();
			}

			act.setNext(nuevo);
			nuevo.setPrev(act);

		}
		else
		{
			primero = nuevo;
			ultimo = nuevo;
		}
	}

	public void removeFirst()
	{
		NodoLista<T> actual = primero;
		NodoLista<T> sig=actual.getNext();

		if (primero !=(null))
		{
			while (actual.getPrev()!=null)
			{
				actual = actual.getPrev();
			} 
			actual.setNext(null);
			sig.setPrev(null);
			
			primero = sig;
			
		}
		else
		{
			primero = null;
		}
	}
	
	public void removeLast()
	{
		NodoLista<T> actual = ultimo;
		NodoLista<T> ant=actual.getPrev();

		if (ultimo !=(null))
		{
			while (actual.getNext()!=null)
			{
				actual = actual.getNext();
			} 
			actual.setPrev(null);
			ant.setNext(null);
			
			ultimo=ant;
		}
		else
		{
			ultimo = null;
		}
	}
	public NodoLista<T> first()
	{
		return primero;
	}

	public NodoLista<T> last()
	{
		return ultimo;
	}

}
