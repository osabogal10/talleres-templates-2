package taller.estructuras;

public class NodoLista<T> {

	private T elem;
	private NodoLista<T> siguiente;
	private NodoLista<T> anterior;
	
	public NodoLista(T pElem)
	{
		elem = pElem;
		siguiente = null;
		elem = pElem;
	}

	 public T getElement()
	 {
	  return elem;
	 }
	 public void setElement(T pElem)
	 {
	  elem = pElem;
	 }
	 public NodoLista<T> getNext()
	 {
	  return siguiente;
	 }
	 public void setNext(NodoLista<T> next)
	 {
	  siguiente = next;
	 }
	 public NodoLista<T> getPrev()
	 {
	  return anterior;
	 }
	 public void setPrev (NodoLista<T> prev)
	 {
	  anterior = prev;
	 }

}
