package taller.mundo;

import java.util.Iterator;

import taller.estructuras.Cola;
import taller.estructuras.ListaDobleEncadenada;

public class AdministracionMcDonalds<T>{

	private ListaDobleEncadenada<Producto> catalogoProductos;
	private Cola<String> colaClientes;
	private Cola<Pedido> colaPedidos;

	public AdministracionMcDonalds()
	{
		colaClientes=new Cola<String>();
		colaPedidos=new Cola<Pedido>();
		catalogoProductos = new ListaDobleEncadenada<Producto>();
		
		catalogoProductos.addFirst( new Producto("Combo Nuggets", 200, 7000));
		catalogoProductos.addLast( new Producto("Combo Bigmac", 300, 14000));
		catalogoProductos.addLast( new Producto("Combo Cuarto de Libra", 250, 9000));
		catalogoProductos.addLast( new Producto("Combo McFlurry", 200, 6000));
	}

	public void agregarCliente(String nombre){
		colaClientes.encolar(nombre);
	}

	public String atenderCliente(Pedido pedido){
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {

		}
		colaPedidos.encolar(pedido);
		return colaClientes.desencolar();
	}

	public String entregarPedido(){
		Pedido p = colaPedidos.desencolar();
		try {
			Thread.sleep(p.darProducto().darTiempo());
		} catch (InterruptedException e) {

		}
		return p.darProducto().darNombre();
	}

	public int clientesEnFila(){
		return colaClientes.tamanio();
	}

	public int pedidosEnEspera(){
		return colaPedidos.tamanio();
	}

	public Iterator<String> clientes(){
		return colaClientes.iterator();
	}

	public Iterator<Pedido> pedidos(){
		return colaPedidos.iterator();
	}

	public ListaDobleEncadenada<Producto> getCatalogoProductos()
	{
		return catalogoProductos;
	}

}
