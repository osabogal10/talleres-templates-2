package taller.mundo;

public class Pedido {

	private long horaPedido;
	private Producto producto;
	private String direccion;
	boolean esDomicilio;
	
	public Pedido(long h,Producto p, String d, boolean e)
	{
		horaPedido=h;
		producto=p;
		direccion=d;
		esDomicilio=e;
	}
	
	public long darHora()
	{
		return horaPedido;
	}
	
	public Producto darProducto()
	{
		return producto;
	}
	
	public String darDireccion()
	{
		return direccion;
	}
	
	public boolean esDomicilio()
	{
		return esDomicilio;
	}
	
}
