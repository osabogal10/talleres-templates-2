package taller.mundo;

public class Producto {

	private String nombre;
	private long tiempo;
	private double costo;

	public Producto ( String pNombre, long pTiempo, double pCosto)
	{
		nombre = pNombre;
		tiempo = pTiempo;
		costo = pCosto;
	}

	public String darNombre()
	{
		return nombre;
	}
	public long darTiempo()
	{
		return tiempo;
	}
	public double darCosto()
	{
		return costo;
	}

}
